#!/bin/bash
set -ex

git clone https://github.com/solvespace/solvespace
cd solvespace
git submodule update --init

mkdir -p build
cd build
cmake .. -DCMAKE_TOOLCHAIN_FILE=../cmake/Toolchain-mingw64.cmake \
         -DCMAKE_BUILD_TYPE=Release
make
