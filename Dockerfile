FROM ubuntu:artful

RUN apt-get update
RUN apt-get install -y git build-essential cmake mingw-w64

COPY builder /
WORKDIR /work
